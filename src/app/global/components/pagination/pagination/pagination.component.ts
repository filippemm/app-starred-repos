import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  @Input() currentPage: number = 1;
  @Output() changePage = new EventEmitter<number>();
  @Input() pageSize: number = 10;
  @Input() totalItems: number = 0;

  public firstPage: number = 1;
  public lastPage: number = 1;
  public pages: Array<{ page: number; current: boolean }> = [{ page: 1, current: true }];

  constructor() {}

  ngOnInit(): void {
    this.setupPages();
  }

  isCurrent(page: number) {
    return page === this.currentPage;
  }

  getPageObject(page: number) {
    return { page, current: this.isCurrent(page) };
  }

  setupPages() {
    this.lastPage = Math.ceil(this.totalItems / this.pageSize);
    const pages: Array<{ page: number; current: boolean }> = [];
    if (this.lastPage === 1) {
      return;
    }
    if (this.lastPage <= 2) {
      pages.push(this.getPageObject(this.firstPage));
      pages.push(this.getPageObject(this.lastPage));
    } else {
      if (this.currentPage - 1 >= this.firstPage) {
        pages.push(this.getPageObject(this.currentPage - 1));
      }
      pages.push(this.getPageObject(this.currentPage));
      if (this.currentPage + 1 <= this.lastPage) {
        pages.push(this.getPageObject(this.currentPage + 1));
      }
    }
    this.pages = pages;
  }

  changeToPage(page: number) {
    if (this.currentPage !== page) {
      this.changePage.emit(page);
    }
  }

  toFistPage() {
    this.changeToPage(this.firstPage);
  }
  toPreviousPage() {
    if (this.currentPage - 1 >= this.firstPage) {
      this.changeToPage(this.currentPage - 1);
    }
  }
  toNextPage() {
    if (this.currentPage + 1 <= this.lastPage) {
      this.changeToPage(this.currentPage + 1);
    }
  }
  toLastPage() {
    this.changeToPage(this.lastPage);
  }
}
