import { Injectable } from '@angular/core';

const AUTHKEY = 'auth_token';
const USERNAMEKEY = 'user_name';
const USERKEY = 'user';
const GITHUBLOADED = 'github_loaded';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  hasToken() {
    return !!this.getToken();
  }

  setToken(token: string) {
    window.localStorage.setItem(AUTHKEY, token);
  }

  getToken() {
    return window.localStorage.getItem(AUTHKEY);
  }

  removeToken() {
    window.localStorage.removeItem(AUTHKEY);
  }

  setUser(userObj: Object) {
    const user = JSON.stringify(userObj);
    if (user) {
      window.localStorage.setItem(USERKEY, user);
    }
  }

  getUser() {
    const user = window.localStorage.getItem(USERKEY);
    if (user) {
      return JSON.parse(user);
    }
    return null;
  }

  removeUser() {
    window.localStorage.removeItem(USERKEY);
  }

  setUsername(userName: string) {
    window.localStorage.setItem(USERNAMEKEY, userName);
  }

  getUsername() {
    return window.localStorage.getItem(USERNAMEKEY);
  }

  removeUsername() {
    window.localStorage.removeItem(USERNAMEKEY);
  }

  setGithubLoaded(loaded: string) {
    window.localStorage.setItem(GITHUBLOADED, loaded);
  }

  getGithubLoaded() {
    return window.localStorage.getItem(GITHUBLOADED);
  }

  removeGithubLoaded() {
    window.localStorage.removeItem(GITHUBLOADED);
  }

  removeAll() {
    this.removeGithubLoaded();
    this.removeToken();
    this.removeUser();
    this.removeUsername();
  }
}
