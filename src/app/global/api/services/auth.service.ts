import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/domains/users/interfaces/user';
import { HttpService } from './http.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends HttpService<User> {
  endpoint = '';

  constructor(protected $http: HttpClient, protected $storageService: StorageService, protected $router: Router) {
    super($http, $storageService, $router);
  }

  login(email: string, password: string) {
    this.endpoint = '/users/authenticate';
    return this.post({ email, password });
  }

  getInfo() {
    this.endpoint = '/oauth/info';
    return this.load();
  }
}
