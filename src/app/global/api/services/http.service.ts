import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpContext, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, first } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export abstract class HttpService<T extends Object> {
  baseUrl: string = environment.backEndUrl;
  abstract endpoint: string;

  constructor(protected $http: HttpClient, protected $storageService: StorageService, protected $router: Router) {}

  post(body?: any, options?: any): Observable<T> {
    return this._request('POST', `${this.baseUrl}${this.endpoint}`, body, options);
  }

  get(id?: string, options?: any): Observable<T> {
    if (id) {
      return this._request('GET', `${this.baseUrl}${this.endpoint}/${id}`, undefined, options);
    }
    return this._request('GET', `${this.baseUrl}${this.endpoint}`, undefined, options);
  }

  load(page: number = 1, order?: string, options?: any): Observable<T> {
    const pagination = page ? `?_page=${page}` : '';
    order = order ? `&_order=${order}` : '';
    return this._request('GET', `${this.baseUrl}${this.endpoint}${pagination}${order}`, undefined, options);
  }

  loadAll(order?: string, options?: any): Observable<T> {
    order = order ? `&_order=${order}` : '';
    return this._request('GET', `${this.baseUrl}${this.endpoint}?_opt=all${order}`, undefined, options);
  }

  put(id: string, body?: any, options?: any): Observable<T> {
    return this._request('PUT', `${this.baseUrl}${this.endpoint}/${id}`, body, options);
  }

  patch(id: string, body?: any, options?: any): Observable<T> {
    return this._request('PATCH', `${this.baseUrl}${this.endpoint}/${id}`, body, options);
  }

  delete(id?: string, body?: any, options?: any): Observable<T> {
    return this._request('DELETE', `${this.baseUrl}${this.endpoint}/${id}`, body, options);
  }

  protected _request(
    method: string,
    endpoint: string,
    body?: undefined,
    options?: {
      body: any;
      headers: HttpHeaders | { [header: string]: string | string[] } | undefined;
      context?: HttpContext | undefined;
      observe?: 'body' | undefined;
      params?:
        | HttpParams
        | { [param: string]: string | number | boolean | readonly (string | number | boolean)[] }
        | undefined;
      reportProgress?: boolean | undefined;
      responseType?: 'arraybuffer';
      withCredentials?: boolean | undefined;
    }
  ): Observable<T> {
    const token = this.$storageService.getToken();
    options = {
      body: body,
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      }),
    };

    return this.$http
      .request(method, endpoint, options)
      .pipe(catchError((err) => this.handleError(err)))
      .pipe(first());
  }

  protected _requestWithoutToken(
    method: string,
    endpoint: string,
    body?: undefined,
    options?: {
      body: any;
      headers: HttpHeaders | { [header: string]: string | string[] } | undefined;
      context?: HttpContext | undefined;
      observe?: 'body' | undefined;
      params?:
        | HttpParams
        | { [param: string]: string | number | boolean | readonly (string | number | boolean)[] }
        | undefined;
      reportProgress?: boolean | undefined;
      responseType?: 'arraybuffer';
      withCredentials?: boolean | undefined;
    }
  ): Observable<T> {
    const token = this.$storageService.getToken();
    options = {
      body: body,
      headers: undefined,
    };

    return this.$http
      .request(method, endpoint, options)
      .pipe(catchError((err) => this.handleError(err)))
      .pipe(first());
  }

  handleError(err: { status: number }): any {
    if (err.status === 401) {
      // Unauthorized
      if (this.$storageService.hasToken()) {
        // Unauthorized
        this.$storageService.removeToken();
        this.$router.navigate(['/login/expired']);
      }
    } else if (err.status === 403) {
      // Forbidden or wrong credentials
      if (this.$storageService.hasToken()) {
        // Unauthorized
        this.$router.navigate(['/dashboard/unauthorized']);
      }
    }

    return throwError(err);
  }
}
