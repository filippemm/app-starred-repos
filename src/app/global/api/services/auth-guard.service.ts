import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor(protected $storageService: StorageService, protected $router: Router) {}

  canActivate() {
    return this.verify();
  }

  canActivateChild() {
    return this.verify();
  }

  verify(): boolean {
    const hasToken = this.$storageService.hasToken();
    if (hasToken) {
      return true;
    }
    this.$router.navigate(['']);
    this.$storageService.removeAll();
    return false;
  }
}
