export interface User {
  name: string;
  email: string;
  password?: string;
  profiles?: string[];
  accessToken?: string;
  githubUsername: string;
}
