export interface GithubUser {
  id: string;
  avatarUrl: string;
  name: string;
  username: string;
  publicRepositories: number;
}
