import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserStorageService } from './services/user-storage.service';
import { ComponentsModule } from './components/components.module';
import { UsersService } from './resources/users.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, ComponentsModule],
  exports: [ComponentsModule],
  providers: [UserStorageService, UsersService],
})
export class UsersModule {}
