import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/global/api/services/auth.service';
import { StorageService } from 'src/app/global/api/services/storage.service';
import { User } from '../../interfaces/user';
import { UsersService } from '../../resources/users.service';

@Component({
  selector: 'app-signin-form',
  templateUrl: './signin-form.component.html',
  styleUrls: ['./signin-form.component.scss'],
})
export class SigninFormComponent implements OnInit {
  @Output() loginEmitter = new EventEmitter();
  @Input() githubUsername: string = '';

  public user: User | undefined;
  public userForm: FormGroup;
  public errorMessage: string = '';

  constructor(
    protected $router: Router,
    protected $userService: UsersService,
    protected $formBuilder: FormBuilder,
    protected $authService: AuthService,
    protected $storageService: StorageService
  ) {
    // TODO verify and remove this first userForm
    this.userForm = this.initForm();
  }

  ngOnInit(): void {
    this.userForm = this.initForm();
  }

  goToLogin() {
    this.loginEmitter.emit();
  }

  protected initForm(): FormGroup {
    return this.$formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(3), Validators.maxLength(80), Validators.required]],
      confirmPassword: ['', [Validators.minLength(3), Validators.maxLength(80), Validators.required]],
      githubUsername: [this.githubUsername, Validators.required],
    });
  }

  protected disableForm(): void {
    this.userForm.disable();
  }

  protected enableForm(): void {
    this.userForm.enable();
  }

  public cleanMessages(): void {
    this.errorMessage = '';
  }

  protected verifyPassword(form: any): boolean {
    const { password, confirmPassword } = form;
    if (password === confirmPassword) {
      return true;
    }
    return false;
  }

  public processForm() {
    const form = this.userForm.getRawValue();
    try {
      this.disableForm();
      if (!this.verifyPassword(form)) {
        this.errorMessage = `Passwords don't match`;
        this.enableForm();
        return;
      }
      form.confirmPassword = undefined;
      this.$userService.post(form).subscribe(
        () => {
          this.login();
        },
        (error) => {
          this.displayErrors(error);
          this.enableForm();
        }
      );
    } catch (error) {
      console.error(error);
      this.enableForm();
    }
  }

  login() {
    const email = this.userForm.get('email')?.value;
    const password = this.userForm.get('password')?.value;

    this.$authService.login(email, password).subscribe(
      (success) => {
        this.$storageService.setToken(success['accessToken'] || '');
        this.$storageService.setUsername(success['name']);
        this.$storageService.setUser(success);
        this.$router.navigate(['dashboard']);
      },
      (error) => {
        if (error.status === 403) {
          this.errorMessage = 'Invalid credentials!';
        } else {
          this.errorMessage = error.error.message;
        }
        this.userForm.get('password')?.reset();
      }
    );
  }

  protected displayErrors(error: { error: { errors: { [x: string]: { message: string } }; message: string } }): void {
    this.cleanMessages();
    if (error.error.errors) {
      for (const name in error.error.errors) {
        this.errorMessage += error.error.errors[name].message;
        this.errorMessage += ' ';
      }
    } else {
      this.errorMessage = error.error.message;
    }
    console.log(error);
  }
}
