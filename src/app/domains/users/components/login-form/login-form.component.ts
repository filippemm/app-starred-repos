import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from 'src/app/global/api/services/auth.service';
import { StorageService } from 'src/app/global/api/services/storage.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @Output() signin = new EventEmitter();

  public loginForm: FormGroup;
  public errorMessage: string = '';

  constructor(
    protected $router: Router,
    protected $route: ActivatedRoute,
    protected $authService: AuthService,
    protected $storageService: StorageService,
    protected $formBuilder: FormBuilder
  ) {
    // TODO verify and remove this first loginForm
    this.loginForm = this.initializeForm();
  }

  ngOnInit() {
    this.initializeForm();
    this.initializeMessage();
  }

  initializeMessage() {
    const reason = this.$route.snapshot.paramMap.get('reason');
    if (reason === 'expired') {
      this.errorMessage = 'Your session has expired, please login again.';
    }
    if (reason === 'logout') {
      if (this.$storageService.hasToken()) {
        this.$storageService.removeToken();
      }
      this.errorMessage = 'You have logged out of the system.';
    }
  }

  initializeForm(): FormGroup {
    return this.$formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
    });
  }

  redirectToLanding() {
    this.$router.navigate(['dashboard']);
  }

  login() {
    const email = this.loginForm.get('email')?.value;
    const password = this.loginForm.get('password')?.value;

    this.$authService.login(email, password).subscribe(
      (success) => {
        this.$storageService.setToken(success['accessToken'] || '');
        this.$storageService.setUsername(success['name']);
        this.$storageService.setUser(success);
        this.redirectToLanding();
      },
      (error) => {
        if (error.status === 403) {
          this.errorMessage = 'Invalid credentials!';
        } else {
          this.errorMessage = error.error.message;
        }
        this.loginForm.get('password')?.reset();
      }
    );
  }

  cleanMessages(): void {
    this.errorMessage = '';
  }

  goToSignin() {
    this.signin.emit();
  }
}
