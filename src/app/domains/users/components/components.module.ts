import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginFormComponent } from './login-form/login-form.component';
import { GithubCheckFormComponent } from './github-check-form/github-check-form.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SigninFormComponent } from './signin-form/signin-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LoginFormComponent, GithubCheckFormComponent, SigninFormComponent],
  imports: [CommonModule, ReactiveFormsModule, HttpClientModule],
  exports: [LoginFormComponent, GithubCheckFormComponent, SigninFormComponent],
  providers: [HttpClient],
})
export class ComponentsModule {}
