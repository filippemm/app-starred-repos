import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { first } from 'rxjs/operators';
import { GithubUser } from '../../interfaces/github-user';
import { GithubApiService } from '../../resources/github-api.service';

@Component({
  selector: 'app-github-check-form',
  templateUrl: './github-check-form.component.html',
  styleUrls: ['./github-check-form.component.scss'],
})
export class GithubCheckFormComponent implements OnInit {
  @Output() login = new EventEmitter();
  @Output() githubUserConfirmed = new EventEmitter<string>();
  public githubUser: GithubUser | undefined = undefined;
  public verifyButton = 'Verify';
  public error = '';

  constructor(protected $githubApiService: GithubApiService) {}

  ngOnInit(): void {}

  goToLogin() {
    this.login.emit();
  }

  async searchGithubUser(username: string) {
    try {
      const githubUser = await this.$githubApiService.getUser(username).pipe(first()).toPromise();
      if (githubUser && githubUser.id) {
        this.githubUser = githubUser;
        this.verifyButton = 'Try Again';
        this.error = '';
      } else {
        this.error = `User "${username}" not found, try again with another username.`;
      }
    } catch (error) {
      this.error = `User "${username}" not found, try again with another username.`;
    }
  }

  confirmGithubUser(githubUser: GithubUser | undefined) {
    if (!githubUser) {
      this.error = 'Something went wrong, try searching Github User again.';
      return;
    }
    this.error = '';
    const { username } = githubUser;
    this.githubUserConfirmed.emit(username);
  }
}
