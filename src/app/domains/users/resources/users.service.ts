import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { HttpService } from '../../../global/api/services/http.service';
import { StorageService } from '../../../global/api/services/storage.service';
import { User } from '../interfaces/user';
@Injectable({
  providedIn: 'root',
})
export class UsersService extends HttpService<User | User[]> {
  endpoint = '/users';

  constructor(protected $http: HttpClient, protected $storageService: StorageService, protected $router: Router) {
    super($http, $storageService, $router);
  }

  post(body?: any, options?: any): Observable<User | User[]> {
    return this._requestWithoutToken('POST', `${this.baseUrl}${this.endpoint}`, body, options);
  }
}
