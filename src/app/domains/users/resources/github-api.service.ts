import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GithubApiService {
  protected baseUrl = 'https://api.github.com/users/';
  constructor(protected $http: HttpClient) {}

  getUser(username: string): Observable<any> {
    const url = `${this.baseUrl}${username}`;
    return this.$http.request<any>('GET', url).pipe(
      map((user) => {
        return {
          id: user.id,
          avatarUrl: user.avatar_url,
          name: user.name,
          username: user.login,
          publicRepositories: user.public_repos,
        };
      })
    );
  }
}
