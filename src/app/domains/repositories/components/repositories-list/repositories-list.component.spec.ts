import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { StorageService } from 'src/app/global/api/services/storage.service';
import { Repository } from '../../interfaces/repository';
import { GithubReposService } from '../../resources/github-repos.service';
import { RepositoriesService } from '../../resources/repositories.service';

import { RepositoriesListComponent } from './repositories-list.component';

class MockRepositoriesService {
  path(id: string, body?: any, options?: any): Observable<Repository> {
    const response: Repository = {
      _id: id,
      user: {
        name: 'test',
        email: 'test@test.com',
        githubUsername: 'test123',
      },
      id: '12345',
      name: 'Angular Road Map',
      description: 'One more test!',
      url: 'https://api.github.com/users/test/angular-road-map',
      tags: [],
    };

    return of(response);
  }
}

class MockGithubReposService {
  getUserStarredRepositories(username: string): Observable<any> {
    const response = [
      {
        id: 85077558,
        name: 'kamranahmedse/developer-roadmap',
        description: 'Roadmap to becoming a web developer in 2021',
        url: 'https://github.com/kamranahmedse/developer-roadmap',
      },
      {
        id: 85077558,
        name: 'olmps/memo',
        description:
          'Memo is an open-source, programming-oriented spaced repetition software (SRS) written in Flutter.',
        url: 'https://github.com/olmps/memo',
      },
    ];
    return of(response);
  }
}

describe('RepositoriesListComponent', () => {
  let component: RepositoriesListComponent;
  let fixture: ComponentFixture<RepositoriesListComponent>;
  let $storageService: StorageService;
  let $repositoriesService: RepositoriesService;
  let $githubReposService: GithubReposService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [RepositoriesListComponent],
      providers: [
        StorageService,
        { provide: RepositoriesService, useClass: MockRepositoriesService },
        { provide: GithubReposService, useClass: MockGithubReposService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoriesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    $storageService = TestBed.inject(StorageService);
    $repositoriesService = TestBed.inject(RepositoriesService);
    $githubReposService = TestBed.inject(GithubReposService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
