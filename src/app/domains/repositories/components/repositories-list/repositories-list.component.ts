import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { StorageService } from 'src/app/global/api/services/storage.service';
import { Repository } from '../../interfaces/repository';
import { GithubReposService } from '../../resources/github-repos.service';
import { RepositoriesService } from '../../resources/repositories.service';

@Component({
  selector: 'app-repositories-list',
  templateUrl: './repositories-list.component.html',
  styleUrls: ['./repositories-list.component.scss'],
})
export class RepositoriesListComponent implements OnInit {
  @Output() isLoading = new EventEmitter<boolean>();
  public githubUsername: string = '';
  public repositories: Repository[] = [];
  public loading: string = 'Loading...';
  public empty: boolean = false;
  public searchText: string = ''; // TODO improve
  public pageInformation: { currentPage: number; totalItems: number; pageSize: number } = {
    currentPage: 1,
    totalItems: 0,
    pageSize: 10,
  };

  constructor(
    protected $storageService: StorageService,
    protected $repositoriesService: RepositoriesService,
    protected $githubReposService: GithubReposService
  ) {}

  ngOnInit(): void {
    this.setGithubUsername();
    this.showRepositories();
  }

  async searchTag(text: string, page: number = 1): Promise<void> {
    this.searchText = text;
    this.pageInformation.currentPage = page;
    this.changeLoading('Searching...');
    const repos = await (<any>this.$repositoriesService.search(text, page).toPromise());
    if (repos) {
      this.repositories = <Repository[]>repos.items;
      this.pageInformation.totalItems = repos._information.count;
      this.pageInformation.pageSize = repos._information.pageSize;
    }
    this.changeLoading('');
  }

  changeLoading(text: string): void {
    if (this.loading !== text) {
      if (text.trim()) {
        this.loading = text;
        this.isLoading.emit(true);
      } else {
        this.loading = '';
        this.isLoading.emit(false);
      }
    }
  }

  setGithubUsername(): void {
    const githubUsername = this.$storageService.getUser()?.githubUsername;
    if (githubUsername) {
      this.githubUsername = githubUsername;
    }
  }

  async showRepositories() {
    const page = this.pageInformation.currentPage;
    const { items, _information } = await (<any>this.$repositoriesService.load(page).toPromise());
    if (items && items.length) {
      this.repositories = items;
      this.pageInformation.totalItems = _information.count;
      this.pageInformation.pageSize = _information.pageSize;
      this.changeLoading('');
      this.$storageService.removeGithubLoaded();
    } else {
      this.saveRepositoriesFromGithub();
      this.changeLoading('Loading From GitHub...');
    }
  }

  async saveRepositoriesFromGithub() {
    const githubUsername = this.githubUsername;
    const githubRepos = await this.$githubReposService.getUserStarredRepositories(githubUsername).toPromise();
    const repositoriesCount = githubRepos.length;
    let index = 0;
    for (const repository of githubRepos) {
      try {
        await this.$repositoriesService.post(repository).toPromise();
      } catch (error) {
        if (error.error.message !== 'This repository has already been registered.') {
          console.log(error);
        }
      }
      index++;
      this.changeLoading(`Loading From GitHub (${index}/${repositoriesCount})...`);
    }

    const githubLoaded = this.$storageService.getGithubLoaded();
    const { email } = this.$storageService.getUser();
    const key = `${githubUsername}${email}`;
    if (githubLoaded !== key) {
      this.$storageService.setGithubLoaded(key);
      window.location.reload();
      return;
    }
    this.empty = true;
    this.changeLoading('');
  }

  async changePage(page: number) {
    this.changeLoading('Loading Page...');
    this.pageInformation.currentPage = page;
    if (this.searchText) {
      await this.searchTag(this.searchText, this.pageInformation.currentPage);
    } else {
      await this.showRepositories();
    }
  }
}
