import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RepositoriesListComponent } from './repositories-list/repositories-list.component';
import { RepositoriesCardComponent } from './repositories-card/repositories-card.component';
import { PaginationModule } from 'src/app/global/components/pagination/pagination.module';

@NgModule({
  declarations: [RepositoriesListComponent, RepositoriesCardComponent],
  imports: [CommonModule, PaginationModule],
  exports: [RepositoriesListComponent, RepositoriesCardComponent],
})
export class ComponentsModule {}
