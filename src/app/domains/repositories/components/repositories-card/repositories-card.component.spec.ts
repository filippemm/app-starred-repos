import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { Repository } from '../../interfaces/repository';
import { RepositoriesService } from '../../resources/repositories.service';

import { RepositoriesCardComponent } from './repositories-card.component';

class MockRepositoriesService {
  path(id: string, body?: any, options?: any): Observable<Repository> {
    const response: Repository = {
      _id: id,
      user: {
        name: 'test',
        email: 'test@test.com',
        githubUsername: 'test123',
      },
      id: '12345',
      name: 'Angular Road Map',
      description: 'One more test!',
      url: 'https://api.github.com/users/test/angular-road-map',
      tags: [],
    };

    return of(response);
  }
}

describe('RepositoriesCardComponent', () => {
  let component: RepositoriesCardComponent;
  let fixture: ComponentFixture<RepositoriesCardComponent>;
  let $repositoriesService: RepositoriesService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [RepositoriesCardComponent],
      providers: [{ provide: RepositoriesService, useClass: MockRepositoriesService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoriesCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    $repositoriesService = TestBed.inject(RepositoriesService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show full repository card', () => {
    const repository = {
      _id: '60f74be4654fg38d51ce379c',
      id: '12345',
      name: 'Angular Road Map',
      description: 'One more test!',
      url: 'https://api.github.com/users/test/angular-road-map',
      tags: ['testAngular'],
    };
    component.repository = repository;

    fixture.detectChanges();

    const repositoryId: HTMLElement = fixture.debugElement.query(By.css('.repository-id')).nativeElement;
    const repositoryName: HTMLElement = fixture.debugElement.query(By.css('.repository-name')).nativeElement;
    const repositoryDescription: HTMLElement = fixture.debugElement.query(
      By.css('.repository-description')
    ).nativeElement;
    const repositoryLink: HTMLElement = fixture.debugElement.query(By.css('.repository-link')).nativeElement;
    const repositoryTags: HTMLElement = fixture.debugElement.query(By.css('.repository-tag-text')).nativeElement;
    expect(repositoryId.innerHTML).toEqual(`#${repository.id}`);
    expect(repositoryName.innerHTML).toEqual(repository.name);
    expect(repositoryDescription.innerHTML).toEqual(repository.description);
    expect(repositoryLink.innerHTML).toEqual(repository.url);
    expect(repositoryTags.innerHTML).toEqual(repository.tags[0]);
  });

  it('should hide input tag', () => {
    const repository = {
      _id: '60f74be4654fg38d51ce379c',
      id: '12345',
      name: 'Angular Road Map',
      description: 'One more test!',
      url: 'https://api.github.com/users/test/angular-road-map',
      tags: ['testAngular'],
    };
    component.repository = repository;

    fixture.detectChanges();

    const inputTag = fixture.debugElement.query(By.css('.input-repository-tag'));
    expect(component.modified).toBeFalse();
    expect(inputTag).toBeNull();
  });

  it('should show input tag', () => {
    const repository = {
      _id: '60f74be4654fg38d51ce379c',
      id: '12345',
      name: 'Angular Road Map',
      description: 'One more test!',
      url: 'https://api.github.com/users/test/angular-road-map',
      tags: ['testAngular'],
    };
    component.repository = repository;

    const addTagButton: HTMLElement = fixture.debugElement.query(By.css('.button-repository-tag')).nativeElement;
    addTagButton.click();
    fixture.detectChanges();

    const inputTag: HTMLElement = fixture.debugElement.query(By.css('.input-repository-tag')).nativeElement;
    expect(inputTag.innerText).toEqual('');
    expect(component.displayTagInput).toBeTrue();
  });

  it('should not show any tags', () => {
    const repository = {
      _id: '60f74be4654fg38d51ce379c',
      id: '12345',
      name: 'Angular Road Map',
      description: 'One more test!',
      url: 'https://api.github.com/users/test/angular-road-map',
      tags: [],
    };
    component.repository = repository;

    fixture.detectChanges();

    const repositoryTags = fixture.debugElement.query(By.css('.repository-tag-text'));
    expect(repositoryTags).toBeNull();
  });

  it('should remove all tags', () => {
    const repository = {
      _id: '60f74be4654fg38d51ce379c',
      id: '12345',
      name: 'Angular Road Map',
      description: 'One more test!',
      url: 'https://api.github.com/users/test/angular-road-map',
      tags: ['testAngular'],
    };
    component.repository = repository;

    fixture.detectChanges();
    const rmTagButton: HTMLElement = fixture.debugElement.query(By.css('.repository-tag-remove')).nativeElement;
    rmTagButton.click();
    fixture.detectChanges();

    const repositoryTags = fixture.debugElement.query(By.css('.repository-tag-text'));
    expect(repositoryTags).toBeNull();
  });
});
