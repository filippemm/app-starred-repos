import { Component, Input, OnInit } from '@angular/core';
import { Repository } from '../../interfaces/repository';
import { RepositoriesService } from '../../resources/repositories.service';

@Component({
  selector: 'app-repositories-card',
  templateUrl: './repositories-card.component.html',
  styleUrls: ['./repositories-card.component.scss'],
})
export class RepositoriesCardComponent implements OnInit {
  @Input() repository: Repository | undefined;
  public disableAll: boolean = false;
  public modified: boolean = false;
  public displayTagInput: boolean = false;

  constructor(protected $repositoriesService: RepositoriesService) {}

  ngOnInit(): void {}

  showTagInput() {
    this.displayTagInput = true;
  }

  repositoryHasTag(tagSearch: string): boolean {
    if (!this.repository) {
      return true;
    }
    return this.repository.tags.some((tag) => tag.toLocaleLowerCase() === tagSearch.toLocaleLowerCase());
  }

  async addTag(tag: string) {
    tag = this.sanitize(tag);
    if (tag) {
      const hasTag = this.repositoryHasTag(tag);
      if (!hasTag) {
        this.repository?.tags.push(tag);
        this.modified = true;
      }
    }
    this.displayTagInput = false;
    if (this.modified) {
      await this.saveRepository();
    }
  }

  sanitize(text: string) {
    return text.trim();
  }

  async removeTag(tag: string) {
    tag = this.sanitize(tag);
    if (!this.repository) {
      return;
    }
    const pos = this.repository.tags.indexOf(tag);
    if (pos !== -1) {
      this.repository.tags.splice(pos, 1);
      this.modified = true;
      await this.saveRepository();
    }
  }

  // TODO put on service?
  async saveRepository() {
    if (!this.repository?._id) {
      return;
    }
    this.disableAll = true;
    const saved = await this.$repositoriesService.patch(this.repository._id, this.repository).toPromise();
    if (saved) {
      this.repository = <Repository>saved;
      this.modified = false;
    }
    this.disableAll = false;
  }
}
