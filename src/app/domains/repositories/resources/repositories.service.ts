import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/global/api/services/http.service';
import { StorageService } from 'src/app/global/api/services/storage.service';
import { Repository } from '../interfaces/repository';

@Injectable({
  providedIn: 'root',
})
export class RepositoriesService extends HttpService<Repository | Repository[]> {
  endpoint = '/repositories';

  constructor(protected $http: HttpClient, protected $storageService: StorageService, protected $router: Router) {
    super($http, $storageService, $router);
  }

  search(query: string, page: number = 1, options?: any): Observable<Object> {
    return this._request('GET', `${this.baseUrl}${this.endpoint}?_search=${query}&_page=${page}`, undefined, options);
  }

  changePage(page: number, options?: any): Observable<Object> {
    return this._request('GET', `${this.baseUrl}${this.endpoint}?_page=${page}`, undefined, options);
  }
}
