import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class GithubReposService {
  protected baseUrl = 'https://api.github.com/users/';

  constructor(protected $http: HttpClient) {}

  // TODO create interface githubRepositories

  getUserStarredRepositories(username: string): Observable<any> {
    const url = `${this.baseUrl}${username}/starred`;
    return this.$http.request<any>('GET', url).pipe(
      map((repositories: Array<any>) => {
        return repositories.map((repository) => {
          return {
            id: repository.id,
            name: repository.full_name,
            description: repository.description,
            url: repository.html_url,
          };
        });
      })
    );
  }
}
