import { User } from '../../users/interfaces/user';

export interface Repository {
  _id?: string;
  user?: User;
  id: string;
  name: string;
  description: string;
  url: string;
  tags: string[];
}
