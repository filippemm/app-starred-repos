import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../global/api/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: async () => (await import('./login/login.module')).LoginModule,
  },
  {
    path: 'signin',
    loadChildren: async () => (await import('./signin/signin.module')).SigninModule,
  },
  {
    path: 'dashboard',
    loadChildren: async () => (await import('./dashboard/dashboard.module')).DashboardModule,
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
