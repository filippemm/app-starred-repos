import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { SigninRoutingModule } from './signin-routing.module';
import { SigninPageComponent } from './signin-page/signin-page.component';
import { UsersModule } from 'src/app/domains/users/users.module';
@NgModule({
  declarations: [SigninPageComponent],
  imports: [CommonModule, SigninRoutingModule, UsersModule],
  providers: [HttpClient],
})
export class SigninModule {}
