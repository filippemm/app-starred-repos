import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin-page',
  templateUrl: './signin-page.component.html',
  styleUrls: ['./signin-page.component.scss'],
})
export class SigninPageComponent implements OnInit {
  public githubUsername = '';

  constructor(protected $router: Router) {}

  ngOnInit(): void {}

  goToLogin() {
    this.$router.navigate(['']);
  }

  setGithubUser(githubUsername: string) {
    if (githubUsername) {
      console.log(githubUsername);

      this.githubUsername = githubUsername;
    }
  }
}
