import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RepositoriesListComponent } from 'src/app/domains/repositories/components/repositories-list/repositories-list.component';
import { StorageService } from 'src/app/global/api/services/storage.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent implements OnInit {
  @ViewChild(RepositoriesListComponent)
  private repositoriesListComponent!: RepositoriesListComponent;
  public githubUsername: string = '';
  public loading: boolean = true;
  public searchTagText: string = '';
  public searchForm: FormGroup;

  constructor(
    protected $router: Router,
    protected $storageService: StorageService,
    protected $formBuilder: FormBuilder
  ) {
    this.searchForm = this.initializeForm();
  }

  ngOnInit(): void {
    this.setGithubUsername();
  }

  initializeForm() {
    return this.$formBuilder.group({ search: [''] });
  }

  setGithubUsername(): void {
    const githubUsername = this.$storageService.getUser()?.githubUsername;
    if (githubUsername) {
      this.githubUsername = githubUsername;
    }
  }

  setLoading(bool: boolean) {
    this.loading = bool;
  }

  async refreshGithubRepositories() {
    this.loading = true;
    await this.repositoriesListComponent.saveRepositoriesFromGithub();
    this.loading = false;
  }

  async searchTag() {
    this.searchTagText = this.searchForm.get('search')?.value;
    await this.repositoriesListComponent.searchTag(this.searchTagText);
  }

  cleanSearchTag() {
    this.searchForm.get('search')?.setValue('');
    this.searchTag();
  }

  logout() {
    this.$storageService.removeAll();
    this.$router.navigate(['']);
  }
}
