import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { RepositoriesModule } from 'src/app/domains/repositories/repositories.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DashboardPageComponent],
  imports: [CommonModule, DashboardRoutingModule, RepositoriesModule, ReactiveFormsModule],
})
export class DashboardModule {}
