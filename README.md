# AppStarredRepos

This is a project that adds tag functionality to its github starred repositories using the [GitHub API](https://docs.github.com/pt/rest).

## Back-End Project

[ApiStarredRepos](https://gitlab.com/filippemm/api-starred-repos)

## Published Website

If you wish to view this project without running it locally, it is online at [https://mystarredrepos.filippemafra.dev/](https://mystarredrepos.filippemafra.dev/). This is a production version running with [Nginx](https://www.nginx.com), SSL with [Let's Encrypt](https://letsencrypt.org/) and [Certbot](https://certbot.eff.org), on a [DigitalOcean](https://m.do.co/c/8c53e4a98a57) droplet. The CI/CD was make with [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

## System Environment

- [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1;
- [Git](https://git-scm.com) 2.30.1 (or higher);
- [Node](https://nodejs.org/) 14.17.3 (or higher).

Take care to verify that the Angular version is compatible with the Node version.

## Development server

### Run development server

1. Install dependencies with `npm install`.

2. Enter your `backEndUrl` in the `src/environments/environment.ts` file.

3. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Login and Signin

See more instructions on [ApiStarredRepos](https://gitlab.com/filippemm/api-starred-repos).

### Run tests

Tests were done with [Karma](https://angular.io/guide/testing-components-basics) for examples and more information in [Angular](https://angular.io/guide/testing-components-basics).

Run `npm test` for a component tests.

## Future plans

- Send in one call all repositories to [ApiStarredRepos](https://gitlab.com/filippemm/api-starred-repos);
- Logout when token expires;
- Possibility to delete repositories when synchronizing repositories;
- Improve mobile experience;
- Signin and login with Github.
